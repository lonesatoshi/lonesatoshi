<?php

define("TIME_STARTUP", microtime(true));
define("APP_ROOT", dirname(__FILE__));
define("APP_NAME", "LoneSatoshi");
define("THEME", "LoneSatoshi");

error_reporting(E_ALL);
ini_set('display_errors', '1');
set_time_limit(120);
if(!file_exists('./vendor/autoload.php')){
  die("You need to run <em>php composer.phar update</em> in the Sous root directory.");
}

require_once("./vendor/autoload.php");
require_once("./vendor/fouroneone/session/FourOneOne/Session/Session.php");
require_once("./src/config/config.php");
require_once("./src/lib/cpu_count.php");
require_once("./src/lib/cron.php");
require_once("./src/lib/mail.php");

function get_or_create_market_valuation(\LoneSatoshi\Models\Market $market, \LoneSatoshi\Models\Coin $to_coin, \LoneSatoshi\Models\Coin $from_coin){
  $market_valuation = \LoneSatoshi\Models\MarketValuation::search()
    ->where('market_id', $market->market_id)
    ->where('to_coin_id', $to_coin->coin_id)
    ->where('from_coin_id', $from_coin->coin_id)
    ->execOne();
  if(!$market_valuation instanceof \LoneSatoshi\Models\MarketValuation){
    $market_valuation = new \LoneSatoshi\Models\MarketValuation();
    $market_valuation->market_id = $market->market_id;
    $market_valuation->to_coin_id = $to_coin->coin_id;
    $market_valuation->from_coin_id = $from_coin->coin_id;
  }
  return $market_valuation;
}

function update_valuation_bittrex(){
  $pricing_url = '/api/v1/public/getmarketsummaries';
  $client = new \Guzzle\Http\Client('https://bittrex.com');
  $request = $client->get($pricing_url);
  $response = $request->send();
  $data = $response->json();
  $valuations = array();
  foreach($data['result'] as $valuation){
    $valuations[$valuation['MarketName']] = $valuation;
  }
  \LoneSatoshi\Models\Setting::set('bittrex_pricing', $valuations);
  $pricing = json_decode(\LoneSatoshi\Models\Setting::get('bittrex_pricing'), true);

  $market = \LoneSatoshi\Models\Market::search()->where('name','Bittrex')->execOne();
  if(!$market instanceof \LoneSatoshi\Models\Market){
    throw new Exception("Cannot find Market Bittrex");
  }
  foreach($pricing as &$pricing_record){
    $pricing_record['Ask'] = number_format($pricing_record['Ask'],8);
    $pricing_record['BaseVolume'] = number_format($pricing_record['BaseVolume'],8);
    $pricing_record['Bid'] = number_format($pricing_record['Bid'],8);
    $pricing_record['High'] = number_format($pricing_record['High'],8);
    $pricing_record['Last'] = number_format($pricing_record['Last'],8);
    $pricing_record['Low'] = number_format($pricing_record['Low'],8);
    $pricing_record['PrevDay'] = number_format($pricing_record['PrevDay'],8);
    $pricing_record['Volume'] = number_format($pricing_record['Volume'],8);
  }
  foreach($pricing as $valuation){
    $name = explode("-", $valuation['MarketName']);
    $to_coin = \LoneSatoshi\Models\Coin::search()->where('symbol', $name[0])->execOne();
    $from_coin = \LoneSatoshi\Models\Coin::search()->where('symbol', $name[1])->execOne();
    if($to_coin instanceof \LoneSatoshi\Models\Coin){
      if($from_coin instanceof \LoneSatoshi\Models\Coin){
        echo "Adding {$to_coin->name}-{$from_coin->name} ";
        $market_valuation = get_or_create_market_valuation($market, $to_coin, $from_coin);
        $market_valuation->updated = date("Y-m-d H:i:s");
        $market_valuation->value = $valuation['Ask'];
        echo "At {$valuation['Ask']} ";
        $market_valuation->save();
        echo "[Done]\n";
      }
    }
  }
}

function update_valuation_blockchaininfo(){

  // Get Data
  $pricing_url = '/ticker';
  $client = new \Guzzle\Http\Client('https://blockchain.info');
  $request = $client->get($pricing_url);
  $response = $request->send();
  $data = $response->json();

  // Get Market
  $market = \LoneSatoshi\Models\Market::search()->where('name','Blockchain.info')->execOne();
  if(!$market instanceof \LoneSatoshi\Models\Market){
    throw new Exception("Cannot find Market Blockchain.info");
  }

  // Get Valuations
  foreach($data as $from => $valuation){
    $to_coin = \LoneSatoshi\Models\Coin::search()->where('symbol', 'BTC')->execOne();
    $from_coin = \LoneSatoshi\Models\Coin::search()->where('symbol', $from)->execOne();

    if($to_coin instanceof \LoneSatoshi\Models\Coin){
      if($from_coin instanceof \LoneSatoshi\Models\Coin){
        echo "Adding {$to_coin->name}-{$from_coin->name} ";
        $market_valuation = get_or_create_market_valuation($market, $to_coin, $from_coin);
        $market_valuation->updated = date("Y-m-d H:i:s");
        $market_valuation->value = $valuation['15m'];
        echo "At {$valuation['15m']} ";
        $market_valuation->save();
        echo "[Done]\n";
      }
    }
  }
}

function update_valuation_poloniex(){
  // Get Data
  $pricing_url = '/public?command=returnTicker';
  $client = new \Guzzle\Http\Client('https://poloniex.com');
  $request = $client->get($pricing_url);
  $response = $request->send();
  $data = $response->json();

  // Get Market
  $market = \LoneSatoshi\Models\Market::search()->where('name','Poloniex')->execOne();
  if(!$market instanceof \LoneSatoshi\Models\Market){
    throw new Exception("Cannot find Market Poloniex");
  }

  // Get Valuations
  foreach($data as $exchange => $valuation){
    $exchange = explode("_", $exchange);

    $to_coin = \LoneSatoshi\Models\Coin::search()->where('symbol', $exchange[0])->execOne();
    $from_coin = \LoneSatoshi\Models\Coin::search()->where('symbol', $exchange[1])->execOne();

    if($to_coin instanceof \LoneSatoshi\Models\Coin){
      if($from_coin instanceof \LoneSatoshi\Models\Coin){
        echo "Adding {$to_coin->name}-{$from_coin->name} ";
        $market_valuation = get_or_create_market_valuation($market, $to_coin, $from_coin);
        $market_valuation->updated = date("Y-m-d H:i:s");
        $market_valuation->value = $valuation['highestBid'];
        echo "At {$valuation['highestBid']} ";
        $market_valuation->save();
        echo "[Done]\n";
      }
    }
  }
}

echo "\n\nBlockchain:\n";
update_valuation_blockchaininfo();

echo "\n\nBittrex:\n";
update_valuation_bittrex();

echo "\n\nPoloniex:\n";
update_valuation_poloniex();