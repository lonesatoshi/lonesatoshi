<?php

$app->get('/wallets', function () use ($app) {
  \LoneSatoshi\Models\User::check_logged_in();
  LoneSatoshi\Api\Base::Startup(\LoneSatoshi\Models\User::get_current()->get_api_key()->api_key);

  $accounts = LoneSatoshi\Api\Wallets::WalletSummary();

  $app->render('wallets/list.phtml', array(
    'accounts' => $accounts
  ));
});


$app->get('/wallets/rename/:account_id', function ($account_id) use ($app) {
  \LoneSatoshi\Models\User::check_logged_in();

  $account = \LoneSatoshi\Models\Account::search()
    ->where('user_id', \LoneSatoshi\Models\User::get_current()->user_id)
    ->where('account_id', $account_id)
    ->execOne();

  $app->render('wallets/rename.phtml', array(
    'account' => $account
  ));
});

$app->post('/wallets/rename/:account_id', function ($account_id) use ($app) {
  \LoneSatoshi\Models\User::check_logged_in();

  $account = \LoneSatoshi\Models\Account::search()
    ->where('user_id', \LoneSatoshi\Models\User::get_current()->user_id)
    ->where('account_id', $account_id)
    ->execOne();

  if(!$account instanceof \LoneSatoshi\Models\Account){
    die("No such account");
  }

  $account->name = $_POST['name'];
  $account->save();

  header("Location: /wallets");
  exit;
});

$app->get('/wallets/add', function () use ($app) {
  \LoneSatoshi\Models\User::check_logged_in();

  $app->render('wallets/add.phtml', array(
    'coins' => \LoneSatoshi\Models\Coin::search()->where('is_fiat', 'No')->exec(),
  ));
});


$app->post('/wallets/add', function () use ($app) {
  \LoneSatoshi\Models\User::check_logged_in();
  $user = \LoneSatoshi\Models\User::get_current();
  /* @var $coin \LoneSatoshi\Models\Coin */
  $coin = \LoneSatoshi\Models\Coin::search()->where('coin_id', $_POST['coin'])->execOne();
  if(!$coin instanceof \LoneSatoshi\Models\Coin){
    die("Coin not found");
  }
  $wallet = $coin->get_wallet();
  if(!$wallet instanceof \LoneSatoshi\Models\Wallet){
    die("Wallet not found");
  }
  $wallet->create_account_in_wallet($user, $coin);
  header("Location: /wallets");
  exit;
});
