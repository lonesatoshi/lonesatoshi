<?php

$app->get('/status', function () use ($app) {
  $app->render('status/display.phtml', array(
    'wallets' => \LoneSatoshi\Models\Wallet::search()->exec()
  ));
});
