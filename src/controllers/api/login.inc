<?php

require_once(dirname(__FILE__) . "/../../lib/json_pretty_print.php");

$app->post('/login', function () use ($app) {
  $response = new StdClass();

  if($app->request()->post('username')){
    // First try the username
    $user = \LoneSatoshi\Models\User::search()
      ->where('username', $app->request()->post('username'))
      ->where('password', sha1($app->request()->post('password')))
      ->execOne();

    // if that fails, try the email.
    if(!$user instanceof \LoneSatoshi\Models\User){
      $user = \LoneSatoshi\Models\User::search()
        ->where('email', $app->request()->post('username'))
        ->where('password', sha1($app->request()->post('password')))
        ->execOne();
    }

    if(!$user instanceof \LoneSatoshi\Models\User){
      $response->Status = 'FAIL';
      $response->FailureMessage = 'Username or Password not valid';
      header("Content-type: application/json");
      echo json_pretty_print($response);
      exit;
    }else{
      // API key object
      $api = \LoneSatoshi\Models\ApiKey::search()
        ->where('user_id', $user->user_id)
        ->where('revoked', 'No')
        ->execOne();

      // If no API object, create one.
      if(!$api instanceof \LoneSatoshi\Models\ApiKey){
        $api = \LoneSatoshi\Models\ApiKey::create($user);
      }
    }
  }elseif($app->request()->post('apikey')){
    $api = \LoneSatoshi\Models\ApiKey::search()
      ->where('api_key', $app->request()->post('apikey'))
      ->where('revoked', 'No')
      ->execOne();
    $user = $api->get_user();
  }else{
    $response->Status = 'FAIL';
    $response->FailureMessage = 'Need to send username/password or apikey';
  }

  if(isset($api) && $api instanceof \LoneSatoshi\Models\ApiKey){
    $session = $api->create_session();
    $response->Status = 'OKAY';
    $response->Session = $session->get_session_array();
    $response->ApiKey = $api->api_key;
    $response->User = array(
      'username' => $user->username,
      'displayname' => $user->displayname,
      'email' => $user->email,
      'created' => $user->created,
    );
  }else{
    $response->Status = 'FAIL';
    $response->FailureMessage = 'API key not valid';
    header("Content-type: application/json");
    echo json_pretty_print($response);
    exit;
  }

  header("Content-type: application/json");
  echo json_pretty_print($response);
  exit;

});
