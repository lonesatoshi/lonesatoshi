<?php

require_once(dirname(__FILE__) . "/../../lib/json_pretty_print.php");

$app->get('/addressbook/:session_key', function ($session_key) use ($app) {

  $response = new StdClass();
  $session = \LoneSatoshi\Models\ApiSession::load($session_key);

  if(!$session instanceof \LoneSatoshi\Models\ApiSession || !$session->get_api_key() instanceof \LoneSatoshi\Models\ApiKey){
    $response->Status = 'FAIL';
    $response->FailureMessage = 'API key not valid or API session expired';
  }else{
    $response->Status = 'OKAY';
    $response->Session = $session->get_session_array();

    $user = $session->get_api_key()->get_user();
    $addresses = array();
    foreach(\LoneSatoshi\Models\AddressBook::search()->where('user_id', $user->user_id)->exec() as $address){
      /* @var $address \LoneSatoshi\Models\AddressBook */
      $addresses[] = array(
        'address_id' => $address->address_book_id,
        'name' => $address->name,
        'coin' => array(
          'name' => $address->get_coin()->name,
          'symbol' => $address->get_coin()->symbol,
          'confirmations_required' => $address->get_coin()->confirmations_required,
        ),
        'address' => $address->address,
        'created' => $address->created,
      );
    }

    $response->Addresses = array_values($addresses);
  }

  header("Content-type: application/json");
  echo json_pretty_print($response);
  exit;

});
