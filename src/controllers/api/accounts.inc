<?php

require_once(dirname(__FILE__) . "/../../lib/json_pretty_print.php");

$app->get('/accounts/:session_key', function ($session_key) use ($app) {

  $response = new StdClass();
  $session = \LoneSatoshi\Models\ApiSession::load($session_key);

  if(!$session instanceof \LoneSatoshi\Models\ApiSession || !$session->get_api_key() instanceof \LoneSatoshi\Models\ApiKey){
    $response->Status = 'FAIL';
    $response->FailureMessage = 'API key not valid or API session expired';
  }else{
    $response->Status = 'OKAY';
    $response->Session = $session->get_session_array();

    $user = $session->get_api_key()->get_user();
    $accounts = array();
    foreach($user->get_accounts() as $account){
      $account_array = $account->get_array();
      // Add to key'd array.
      $key = number_format((is_numeric($account_array['amount']['confirmed_btc']) ? $account_array['amount']['confirmed_btc'] : 0), 8) . "_" . $account->account_id;
      $accounts[$key] = $account_array;
    }

    ksort($accounts);
    $accounts = array_reverse($accounts);
    $response->Accounts = array_values($accounts);
  }

  header("Content-type: application/json");
  echo json_pretty_print($response);
  exit;

});
