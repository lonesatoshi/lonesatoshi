<?php

require_once(dirname(__FILE__) . "/../../lib/json_pretty_print.php");

$app->post('/pay/:session_key', function ($session_key) use ($app) {

  $response = new StdClass();
  $session = \LoneSatoshi\Models\ApiSession::load($session_key);

  if(!$session instanceof \LoneSatoshi\Models\ApiSession || !$session->get_api_key() instanceof \LoneSatoshi\Models\ApiKey){
    $response->Status = 'FAIL';
    $response->FailureMessage = 'API key not valid or API session expired';
  }else{
    $response->Status = 'OKAY';
    $response->Session = $session->get_session_array();
    $response->Post = $app->request()->post();

    $user = $session->get_api_key()->get_user();

    // Get Account ID
    $account_to_pay_from_id = $app->request()->post("account_id");
    if(!is_numeric($account_to_pay_from_id)){
      $response->Status = 'FAIL';
      $response->FailureMessage = 'Account ID not valid';
    }

    // Get Account from ID
    /* @var $account_to_pay_from \LoneSatoshi\Models\Account */
    $account_to_pay_from = \LoneSatoshi\Models\Account::search()
      ->where('account_id', $account_to_pay_from_id)
      ->where('user_id', $user->user_id)
      ->execOne();

    // Check account valid.
    if(!$account_to_pay_from instanceof \LoneSatoshi\Models\Account){
      $response->Status = 'FAIL';
      $response->FailureMessage = 'Account not found';
      header("Content-type: application/json");
      echo json_pretty_print($response);
      exit;
    }
    $response->Account = $account_to_pay_from->get_array();

    // Get coin and wallet.
    $coin = $account_to_pay_from->get_coin();
    $wallet = $coin->get_wallet();

    // Get Amount.
    $amount_to_pay = $app->request()->post("amount");
    if(!is_numeric($amount_to_pay) || !$amount_to_pay > 0){
      $response->Status = 'FAIL';
      $response->FailureMessage = 'Amount is not valid';
      header("Content-type: application/json");
      echo json_pretty_print($response);
      exit;
    }

    // Get Destination
    $destination = $app->request()->post("destination");
    if(!$destination){
      $response->Status = 'FAIL';
      $response->FailureMessage = "No destination passed.";
      header("Content-type: application/json");
      echo json_pretty_print($response);
      exit;
    }

    // Validate Destination
    if(!$wallet->verify_address($destination)){
      $response->Status = 'FAIL';
      $response->FailureMessage = "Target Address Invalid for coin {$coin->name}";
      header("Content-type: application/json");
      echo json_pretty_print($response);
      exit;
    }

    // Check account has enough money
    if($amount_to_pay > $account_to_pay_from->get_balance_confirmed()->balance){
      $response->Status = 'FAIL';
      $response->FailureMessage = 'Not enough money in account.';
      header("Content-type: application/json");
      echo json_pretty_print($response);
      exit;
    }

    $daemon_response = $account_to_pay_from->get_balance_confirmed()->pay($account_to_pay_from->get_coin(), $destination, $amount_to_pay);
    $response->TXID = $daemon_response;

    $wallet->update_transaction_log();

    /* @var $transaction \LoneSatoshi\Models\Transaction */
    $transaction = \LoneSatoshi\Models\Transaction::search()->where('txid', $response->TXID)->execOne();
    $account_to_pay_from->reload();
    $response->Account = $account_to_pay_from->get_array();
    $response->Transaction = $transaction->get_transaction_array();
  }

  header("Content-type: application/json");
  echo json_pretty_print($response);
  exit;

});
