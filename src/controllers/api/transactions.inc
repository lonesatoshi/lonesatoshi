<?php

require_once(dirname(__FILE__) . "/../../lib/json_pretty_print.php");

$app->get('/transactions/:account_id/:session_key', function ($account_id, $session_key) use ($app) {

  $response = new StdClass();
  $session = \LoneSatoshi\Models\ApiSession::load($session_key);

  if(!$session instanceof \LoneSatoshi\Models\ApiSession || !$session->get_api_key() instanceof \LoneSatoshi\Models\ApiKey){
    $response->Status = 'FAIL';
    $response->FailureMessage = 'API key not valid or API session expired';
  }else{
    $response->Status = 'OKAY';
    $response->Session = $session->get_session_array();
    /* @var $account \LoneSatoshi\Models\Account */
    $account = \LoneSatoshi\Models\Account::search()
      ->where('user_id', $session->get_api_key()->get_user()->user_id)
      ->where('account_id', $account_id)
      ->execOne();
    $response->Account = array(
      'id' => $account->account_id,
      'name' => $account->name,
      'created' => $account->created,
    );
    $response->Transactions = array();

    /* @var $account \LoneSatoshi\Models\Account */
    foreach($account->get_transactions() as $transaction){
      $response->Transactions[] = $transaction->get_transaction_array();
    }

  }

  header("Content-type: application/json");
  echo json_pretty_print($response);
  exit;

});
