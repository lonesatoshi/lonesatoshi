<?php

function halt($message){
  $time_to_exec = number_format(microtime(true) - TIME_STARTUP, 3);
  $trace = debug_backtrace();
  die("Complete in {$time_to_exec} sec with message '{$message}' at {$trace[0]['file']}{$trace[0]['line']}");
}