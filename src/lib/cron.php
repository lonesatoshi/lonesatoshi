<?php


function cron(){
  $run = false;
  if(\LoneSatoshi\Models\User::get_current() instanceof \LoneSatoshi\Models\User){
    if(\LoneSatoshi\Models\User::get_current()->is_admin()){
      $run = true;
    }
  }
  $cron_last_run = \LoneSatoshi\Models\Setting::get("cron_last_run");
  echo "Date now       : " . date("Y-m-d H:i:s") . "\n";
  echo "Date last run  : " . date("Y-m-d H:i:s", $cron_last_run) . "\n";
  echo "Last cron took : " . number_format(\LoneSatoshi\Models\Setting::get("cron_execution_time"),3) . " seconds" . "\n";
  if((time() - $cron_last_run) > 30){
    $run = true;
  }

  echo "Last cron run  : " . (time() - $cron_last_run) . " seconds ago \n";
  if($run){
    $cron_start = microtime(true);
    foreach(\LoneSatoshi\Models\Wallet::search()->exec() as $wallet){
      /* @var $wallet \LoneSatoshi\Models\Wallet */
      echo "Updating {$wallet->get_coin()->name} Status...\n";
      $wallet->update_status();
      echo "Updating {$wallet->get_coin()->name} Transaction Log\n";
      $wallet->update_transaction_log();

      $block_count = $wallet->get_info('blocks');
      echo "{$wallet->get_coin()->name} Block count is {$block_count} \n";
      \LoneSatoshi\Models\Setting::set("block_count_" . $wallet->get_coin()->name, $block_count);
    }

    $week_ago = strtotime('1 week ago');
    $week_ago_str = date("Y-m-d H:i:s", $week_ago);

    \FourOneOne\ActiveRecord\DatabaseLayer::get_instance()->passthru("DELETE FROM network_peers WHERE last_recv < {$week_ago}");
    echo "Deleted old network peers\n";

    \FourOneOne\ActiveRecord\DatabaseLayer::get_instance()->passthru("DELETE FROM notifications WHERE created < '{$week_ago_str}'");
    echo "Deleted old notifications\n";

    \FourOneOne\ActiveRecord\DatabaseLayer::get_instance()->passthru("DELETE FROM wallet_actions WHERE time < '{$week_ago_str}'");
    echo "Deleted old wallet actions\n";

    $cron_end = microtime(true);
    $exec_time = $cron_end - $cron_start;
    \LoneSatoshi\Models\Setting::set('cron_execution_time', $exec_time);
    \LoneSatoshi\Models\Setting::set("cron_last_run", time());

    echo "Cron completed in {$exec_time}\n";
  }else{
    echo "Too soon to run cron again.\n";
  }

  exit;
}

function cron_update_peer_log(){
  echo "Updating peer log...\n";
  foreach(\LoneSatoshi\Models\Wallet::search()->exec() as $wallet){
    /* @var $wallet \LoneSatoshi\Models\Wallet */
    $wallet->update_peer_log();
  }
  echo "Done.\n";
}

function cron_update_valuations(){
  $output = '';
  $updated = \ExchangeApi\Valuations::fetch();
  $time_updated = date("Y-m-d H:i:s");

  $existing_batch = \LoneSatoshi\Models\ValuationBatch::search()->where('updated', date('Y-m-d H:i:s', time() - 60*5))->execOne();

  if($updated && !$existing_batch instanceof \LoneSatoshi\Models\ValuationBatch){
    $data = \ExchangeApi\Valuations::get_data();

    $batch = new \LoneSatoshi\Models\ValuationBatch();
    $batch->updated = $time_updated;
    $batch->save(true);


    // Add real valuations
    foreach($data as $source_name => $source_data){
      echo "Adding valuations from {$source_name}\n";
      foreach($source_data as $from_key => $to){
        foreach($to as $to_key => $data){
          $valuation = new \LoneSatoshi\Models\Valuation();
          $valuation->valuation_batch_id = $batch->valuation_batch_id;
          $valuation->source = $source_name;
          $valuation->from = $from_key;
          $valuation->to = $to_key;
          $valuation->value = $data['price'];
          $valuation->updated = $time_updated;
          $valuation->save(false);
          echo " > {$source_name} - {$from_key} => {$to_key} ({$valuation->value})\n";
        }
      }
    }
    echo "\n";

    echo "Adding dummy valuations... ";
    // Add dummy valuations
    $coins_to_make_dummies_of = array();
    $sources_to_make_dummies_of = array();
    foreach(\LoneSatoshi\Models\Valuation::search()->where('valuation_batch_id', $batch->valuation_batch_id)->exec() as $valuation){
      /* @var $valuation \LoneSatoshi\Models\Valuation */
      $coins_to_make_dummies_of[] = $valuation->from;
      $coins_to_make_dummies_of[] = $valuation->to;
      $sources_to_make_dummies_of[] = $valuation->source;
    }
    $coins_to_make_dummies_of = array_unique($coins_to_make_dummies_of);
    $sources_to_make_dummies_of = array_unique($sources_to_make_dummies_of);
    foreach($coins_to_make_dummies_of as $dummy_coin){
      foreach($sources_to_make_dummies_of as $dummy_source){
        $dummy_valuation = new \LoneSatoshi\Models\Valuation();
        $dummy_valuation->valuation_batch_id = $batch->valuation_batch_id;
        $dummy_valuation->source = $dummy_source;
        $dummy_valuation->from = $dummy_coin;
        $dummy_valuation->to = $dummy_coin;
        $dummy_valuation->value = 1.0;
        $dummy_valuation->updated = $time_updated;
        $dummy_valuation->is_dummy = "Yes";
        $dummy_valuation->save();
      }
    }
    echo "[Done]\n";
  }
  echo $output;
}