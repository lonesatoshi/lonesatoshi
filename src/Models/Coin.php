<?php
namespace LoneSatoshi\Models;

class Coin extends \FourOneOne\ActiveRecord\ActiveRecord{
  protected $_table = "coins";

  public $coin_id;
  public $name;
  public $symbol;
  public $auto_generate_wallet = "No";
  public $chain_url_format_address;
  public $chain_url_format_transaction;
  public $chain_name;
  public $confirmations_required = 10;
  public $is_shitcoin = 'No';
  public $is_fiat = 'No';

  /**
   * @return Wallet
   */
  public function get_wallet(){
    $wallet = Wallet::search()->where('coin_id', $this->coin_id)->execOne();
    return $wallet;
  }

  public function convert($target_coin_symbol, $amount){
    $target_coin_symbol = strtoupper($target_coin_symbol);
    /* @var $to_coin Coin */
    $to_coin = Coin::search()->where('symbol', $target_coin_symbol)->execOne();


    // If we're trying to transform to the same currency...
    if($to_coin->coin_id == $this->coin_id){
      return $amount;
    }

    // If we found a valuation, return it.
    $market_valuation = MarketValuation::search()
      ->where('from_coin_id', $this->coin_id)
      ->where('to_coin_id', $to_coin->coin_id)
      ->execOne();
    if($market_valuation instanceof MarketValuation){
      return number_format($amount * $market_valuation->value, 8);
    }

    // Try via Bitcoin.
    $bitcoin = Coin::search()->where('symbol', 'BTC')->execOne();
    $from_coin_to_bitcoin = MarketValuation::search()
      ->where('from_coin_id', $this->coin_id)
      ->where('to_coin_id', $bitcoin->coin_id)
      ->execOne();
    $to_coin_to_bitcoin = MarketValuation::search()
      ->where('from_coin_id', $to_coin->coin_id)
      ->where('to_coin_id', $bitcoin->coin_id)
      ->execOne();
    if($from_coin_to_bitcoin instanceof MarketValuation && $to_coin_to_bitcoin instanceof MarketValuation){
      $amount_in_btc = $from_coin_to_bitcoin->value * $amount;
      $amount_in_target = $amount_in_btc * $to_coin_to_bitcoin->value;
      return number_format($amount_in_target, 8);
    }

    // Try via Litecoin
    $litecoin = Coin::search()->where('symbol', 'LTC')->execOne();
    $from_coin_to_litecoin = MarketValuation::search()
      ->where('from_coin_id', $this->coin_id)
      ->where('to_coin_id', $litecoin->coin_id)
      ->execOne();
    $to_coin_to_litecoin = MarketValuation::search()
      ->where('from_coin_id', $to_coin->coin_id)
      ->where('to_coin_id', $litecoin->coin_id)
      ->execOne();
    if($from_coin_to_litecoin instanceof MarketValuation && $to_coin_to_litecoin instanceof MarketValuation){
      $amount_in_ltc = $from_coin_to_litecoin->value * $amount;
      $amount_in_target = $amount_in_ltc * $to_coin_to_litecoin->value;
      return number_format($amount_in_target, 8);
    }

    // Try via backwards.
    $backwards_market_valuation = MarketValuation::search()
      ->where('to_coin_id', $this->coin_id)
      ->where('from_coin_id', $to_coin->coin_id)
      ->execOne();
    if($backwards_market_valuation instanceof MarketValuation){
      return number_format($amount * $backwards_market_valuation->value, 8);
    }

    return false;
  }
}