<?php
namespace LoneSatoshi\Models;

class MarketValuationHistorical extends \FourOneOne\ActiveRecord\ActiveRecord{
  protected $_table = "market_valuations_history";

  public $market_valuation_id;
  public $market_id;
  public $from_coin_id;
  public $to_coin_id;
  public $value;
  public $as_of;

}