<?php
namespace LoneSatoshi\Models;

class Market extends \FourOneOne\ActiveRecord\ActiveRecord{
  protected $_table = "markets";

  public $market_id;
  public $name;
}