<?php
namespace LoneSatoshi\Models;

class Account extends \FourOneOne\ActiveRecord\ActiveRecord{
  protected $_table = "accounts";

  public $account_id;
  public $user_id;
  public $reference_id;
  public $address;
  public $name;
  public $created;
  public $coin_id;

  private $_coin;
  private $_transactions;

  /**
   * @return BalanceConfirmed
   */
  public function get_balance_confirmed(){
    $balance = BalanceConfirmed::search()->where('account_id', $this->account_id)->execOne();
    return $balance;
  }

  /**
   * @return BalanceUnconfirmed
   */
  public function get_balance_unconfirmed(){
    $balance = BalanceUnconfirmed::search()->where('account_id', $this->account_id)->execOne();
    return $balance;
  }

  /**
   * @return Coin
   */
  public function get_coin(){
    if(!$this->_coin){
      $this->_coin = Coin::search()->where('coin_id', $this->coin_id)->execOne();
    }
    return $this->_coin;
  }

  /**
   * @return Transaction[]
   */
  public function get_transactions(){
    if(!$this->_transactions){
      $this->_transactions = Transaction::search()
        ->where('account_id', $this->account_id)
        ->order('date','DESC')
        ->exec();
    }
    return $this->_transactions;
  }

  /**
   * @return User
   */
  public function get_user(){
    return User::search()->where('user_id', $this->user_id)->execOne();
  }

  public function refresh(){
    $wallet = $this->get_coin()->get_wallet();
    return $wallet->update_transaction_log($this);
  }

  public function get_array(){
    $transactions = array();
    $rates = array(
      '6 hours' => array(),
      '24 hours' => array(),
      '7 days' => array(),
      '30 days' => array()
    );

    foreach($this->get_transactions() as $transaction){
      /* @var $transaction \LoneSatoshi\Models\Transaction */
      $transactions[] = array(
        'amount' => $transaction->amount,
        'amount_btc' => $transaction->convert('btc'),
        'satoshi_rate' => $transaction->get_account()->get_coin()->convert('btc', 1),
        'confirmations' => $transaction->confirmations,
        'category' => $transaction->category,
        'date' => $transaction->date,
        'txid' => $transaction->txid,
        'block' => array(
          'hash' => $transaction->block_hash,
          'index' => $transaction->block_index,
          'time' => $transaction->block_time,
        ),
      );
      foreach(array_keys($rates) as $rate){
        if(strtotime($transaction->date) >= strtotime($rate . " ago")){
          $rates[$rate][] = $transaction->amount;
        }
      }
    }

    // Post-process rates.
    foreach($rates as $name => &$values){
      $values = array_sum($values);
    }
    
    // Work out Confirmed/Unconfirmed balances
    $confirmed = $this->get_balance_confirmed() instanceof \LoneSatoshi\Models\BalanceConfirmed ? $this->get_balance_confirmed()->balance : 0;
    $unconfirmed = $this->get_balance_unconfirmed() instanceof \LoneSatoshi\Models\BalanceUnconfirmed ? $this->get_balance_unconfirmed()->balance : 0;
    $confirmed_btc = $this->get_coin()->convert('btc', $confirmed);
    $unconfirmed_btc = $this->get_coin()->convert('btc', $unconfirmed);
    $confirmed_gbp = $this->get_coin()->convert('gbp', $confirmed);
    $unconfirmed_gbp = $this->get_coin()->convert('gbp', $unconfirmed);
    $confirmed_eur = $this->get_coin()->convert('eur', $confirmed);
    $unconfirmed_eur = $this->get_coin()->convert('eur', $unconfirmed);
    $confirmed_usd = $this->get_coin()->convert('usd', $confirmed);
    $unconfirmed_usd = $this->get_coin()->convert('usd', $unconfirmed);

    return array(
      'account_id' => $this->account_id,
      'name' => $this->name != null ? $this->name : 'Untitled Wallet',
      'address' => $this->address,
      'coin' => array(
        'name' => $this->get_coin()->name,
        'symbol' => $this->get_coin()->symbol,
        'confirmations_required' => $this->get_coin()->confirmations_required,
      ),
      'rate' => $rates,
      'amount' => array(
        'confirmed' => $confirmed,
        'unconfirmed' => $unconfirmed,
        'confirmed_btc' => $confirmed_btc,
        'unconfirmed_btc' => $unconfirmed_btc,
        'confirmed_gbp' => $confirmed_gbp,
        'unconfirmed_gbp' => $unconfirmed_gbp,
        'confirmed_eur' => $confirmed_eur,
        'unconfirmed_eur' => $unconfirmed_eur,
        'confirmed_usd' => $confirmed_usd,
        'unconfirmed_usd' => $unconfirmed_usd,
        'satoshi_rate' => $this->get_coin()->convert('btc', 1),
      ),

      'transactions' => $transactions,
    );
  }

}