<?php
namespace LoneSatoshi\Models;

class SystemStatus extends \FourOneOne\ActiveRecord\ActiveRecord{
  protected $_table = "system_statuses";

  public $system_status_id;
  public $system;
  public $status;
  public $created;

  private function is_json(){
    if(json_decode($this->status) != null){
      return true;
    }
    return false;
  }

  public function get_status(){
    if($this->is_json()){
      $json = json_decode($this->status);
      return "Block: {$json->blocks}";
    }elseif(strlen($this->status) > 0){
      return $this->status;
    }else{
      return "NO STATUS AVAILABLE.. WUHOH!";
    }
  }
}