<?php

namespace LoneSatoshi\Models;

class Setting extends \FourOneOne\ActiveRecord\ActiveRecord{
  protected $_table = "settings";

  public $setting_id;
  public $name;
  public $user_id;
  public $value;
  public $created;

  static public function set($name, $value, $user_id = null){
    $setting = Setting::search()->where('name', $name)->execOne();
    if(!$setting instanceof Setting){
      $setting = new Setting();
    }
    $setting->name = $name;
    $setting->value = $value;
    $setting->user_id = $user_id;
    $setting->created = date("Y-m-d H:i:s");
    $setting->save();
  }

  static public function get($name, $user_id = null, $default = null, $max_age_sec = null){
    $setting_query = Setting::search();
    $setting_query->where('name', $name);
    if($user_id != null){
      $setting_query->where('user_id', $user_id);
    }
    if($max_age_sec !== null){
      $created_max_age = date("Y-m-d", time() - $max_age_sec);
      $setting_query->where('created', $created_max_age, '>=');
    }
    $setting = $setting_query->execOne();
    if($setting instanceof Setting){
      return $setting->value;
    }
    if($default !== null){
      return $default;
    }
    return false;
  }
}