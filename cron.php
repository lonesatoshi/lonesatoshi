<?php

define("TIME_STARTUP", microtime(true));
define("APP_ROOT", dirname(__FILE__));
define("APP_NAME", "LoneSatoshi");
define("THEME", "LoneSatoshi");

error_reporting(E_ALL);
ini_set('display_errors', '1');
set_time_limit(120);
if(!file_exists('./vendor/autoload.php')){
  die("You need to run <em>php composer.phar update</em> in the Sous root directory.");
}

require_once("./vendor/autoload.php");
require_once("./vendor/fouroneone/session/FourOneOne/Session/Session.php");
require_once("./src/config/config.php");
require_once("./src/lib/cpu_count.php");
require_once("./src/lib/cron.php");
require_once("./src/lib/mail.php");

cron();